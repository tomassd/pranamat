//Swiper bar
//speed:400 closes bugs on chrome/windows
//
var kaSwiper1 = new Swiper ('.swiper-container', {
	slidesPerView: 3,
	spaceBetween: 0,
	slidesPerGroup: 1,
  loop: true,
	loopFillGroupWithBlank: true,
  pagination: '.swiper-pagination',
  paginationClickable: true,
  centeredSlides: true,
  slidesPerView: 'auto',
  spaceBetween : 0,
  autoHeight: true,
	nextButton: '.swiper-button-next',
 	prevButton: '.swiper-button-prev',
	speed: 600,
  breakpoints :{
    768:{
      spaceBetweenSlides: 0
    }
  }
});
